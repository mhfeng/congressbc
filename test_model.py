import model
import numpy as np
import pytest

test_opinion_1 = np.zeros((10,1))
test_opinion_2 = np.ones((10,1))
test_tol = 1e-16

test_adjacency_matrix = np.ones((5,5))
test_opinion_3 = np.array([[0], [1], [0], [0], [1]])

test_model = model.CongressionalOpinionModel.build_congressional_opinion_model(1, test_adjacency_matrix, .5,
                                                                               test_opinion_3, test_tol, 10)

test_opinion_2d = np.array([[1, 0],
                            [0, 0],
                            [1,1]])
test_model_2d = model.CongressionalOpinionModel.build_congressional_opinion_model(1, np.ones((3,3)), .5,
                                                                                  test_opinion_2d, test_tol, 10)

test_model_convergence = model.CongressionalOpinionModel.build_congressional_opinion_model(.2, test_adjacency_matrix, 2,
                                                                                           test_opinion_3, test_tol, 10)

test_model_2d_convergence = model.CongressionalOpinionModel.build_congressional_opinion_model(.2, np.ones((3,3)),
                                                                                              2, test_opinion_2d,
                                                                                              test_tol, 100)

test_adjacency_matrix_divergence = np.array([[1, 1, -1], [1, 1, -1], [-1, -1, 1]])

def test_model_validity():
    with pytest.raises(ValueError):
        model.CongressionalOpinionModel.build_congressional_opinion_model(1, np.ones((3,3)), .5, test_opinion_1,
                                                                          test_tol, 1)

def test_has_opinion_converged():
    assert model.has_opinion_converged(test_opinion_1, test_opinion_1, test_tol)
    assert not model.has_opinion_converged(test_opinion_1, test_opinion_2, test_tol)

def test_build_receptivity_matrix():
    test_receptivity_matrix = np.array([[1, 0, 1, 1, 0],
                                        [0, 1, 0, 0, 1],
                                        [1, 0, 1, 1, 0],
                                        [1, 0, 1, 1, 0],
                                        [0, 1, 0, 0, 1]])
    assert np.array_equal(model.build_receptivity_matrix(test_model), test_receptivity_matrix)

    test_receptivity_matrix = np.eye(3)
    assert np.array_equal(model.build_receptivity_matrix(test_model_2d), test_receptivity_matrix)

def test_compute_updated_opinions():
    assert np.array_equal(model.compute_updated_opinions(test_model), test_opinion_3)
    assert np.array_equal(model.compute_updated_opinions(test_model_2d), test_opinion_2d)
    assert np.allclose(model.compute_updated_opinions(test_model_convergence),
                       np.array([[0.4], [0.4], [0.4], [0.4], [0.4]]))
    assert np.allclose(model.compute_updated_opinions(test_model_2d_convergence),
                       np.array([[0.8, 0.2], [0.4, 0.2], [0.8, 0.6]]))

def test_find_opinion_steady_state():
    assert np.array_equal(model.find_opinion_steady_state(test_model), test_opinion_3)
    assert np.array_equal(model.find_opinion_steady_state(test_model_2d), test_opinion_2d)
    assert np.allclose(model.find_opinion_steady_state(test_model_convergence),
                       np.array([[0.4], [0.4], [0.4], [0.4], [0.4]]))
    assert np.allclose(model.find_opinion_steady_state(test_model_2d_convergence),
                       np.array([[2/3, 1/3], [2/3, 1/3], [2/3, 1/3]]))

# README #

### What is this repository for? ###

This repository contains code to run simulations for the bounded confidence model described in (CITE)
* Version 1.0

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Questions about the repository can be directed to mfeng@caltech.edu
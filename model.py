import numpy as np
import uuid
from scipy.spatial.distance import pdist, squareform


class CongressionalOpinionModel:
    def __init__(self, susceptibility, adjacency_matrix, confidence, initial_opinions, tol, stopping_time):
        self.id = uuid.uuid4()
        self.susceptibility = susceptibility
        self.adjacency = adjacency_matrix
        self.confidence = confidence
        self.initial_opinions = initial_opinions
        self.current_opinions = initial_opinions.copy()
        self.tol = tol
        self.stopping_time = stopping_time

    @staticmethod
    def build_congressional_opinion_model(susceptibility, adjacency_matrix, confidence,
                                          initial_opinions, tol, stopping_time):
        if adjacency_matrix.shape[0] != initial_opinions.shape[0]:
            raise ValueError("The number of individuals with opinions and the number of individuals "
                             "in the adjacency matrix should be the same")
        return CongressionalOpinionModel(susceptibility, adjacency_matrix, confidence,
                                         initial_opinions, tol, stopping_time)


def build_receptivity_matrix(model):
    distances = pdist(model.current_opinions)
    return squareform(distances) < model.confidence


def compute_updated_opinions(model):
    receptivity_matrix = build_receptivity_matrix(model)
    update_matrix = np.multiply(model.adjacency, receptivity_matrix.astype(int))
    update_matrix = update_matrix.astype(int)
    num_entries = np.sum(update_matrix, axis=0)
    updated_opinions = model.current_opinions \
                       + model.susceptibility * (np.matmul(update_matrix, model.current_opinions)
                                                 - np.matmul(np.diag(num_entries), model.current_opinions))
    return updated_opinions


def has_opinion_converged(current_opinions, new_opinions, tol):
    if np.linalg.norm(new_opinions - current_opinions) < tol:
        return True
    else:
        return False


def find_opinion_steady_state(model, retain_history = True):
    time = 0
    new_opinions = compute_updated_opinions(model)
    has_opinion_converged_flag = False
    if retain_history:
        opinion_history = model.initial_opinions

    while ((not has_opinion_converged_flag) and (time < model.stopping_time)):
        new_opinions = compute_updated_opinions(model)
        if has_opinion_converged(model.current_opinions, new_opinions, model.tol):
            has_opinion_converged_flag = True
        model.current_opinions = new_opinions
        if retain_history:
            opinion_history = np.append(opinion_history, new_opinions, axis=1)
        time += 1

    if retain_history:
        return opinion_history
    else:
        return new_opinions

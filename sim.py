from model_io import read_adjacency_matrix, write_model_to_json
from initial_opinion import generate_initial_opinion_from_random_uniform
from model import CongressionalOpinionModel, find_opinion_steady_state
import datetime
import os

def run_single_model(path_to_adjacency, write_model=True):
    adjacency_matrix = read_adjacency_matrix(path_to_adjacency)
    N = adjacency_matrix.shape[0]
    initial_opinions = generate_initial_opinion_from_random_uniform(N)
    tol = 1e-16
    stopping_time = 1e4

    model = CongressionalOpinionModel(.001, adjacency_matrix, .5, initial_opinions, tol, stopping_time)
    model_dict = {'susceptibility': model.susceptibility, 'adjacency': model.adjacency.tolist(),
                  'confidence': model.confidence, 'initial_opinions': model.initial_opinions.tolist(), 'tol': model.tol,
                  'stopping_time': model.stopping_time, 'history': find_opinion_steady_state(model).tolist()}

    if write_model:
        output_folder = 'sims/' + str(datetime.date.today()) + '/'
        if not os.path.isdir(output_folder):
            os.mkdir(output_folder)
        write_model_to_json(model, model_dict, output_folder)

def run_sims(n, path_to_adjacency):
    for i in range(n):
        run_single_model(path_to_adjacency)
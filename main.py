from model import CongressionalOpinionModel, find_opinion_steady_state
from initial_opinion import generate_initial_opinion_from_random_uniform
from model_io import read_adjacency_matrix, write_model_to_json
from sim import run_sims

import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt

MODEL_BY_MODEL_ID = {}
OPINION_TIMELINE_BY_MODEL_ID = {}

if __name__ == '__main__':
    path_to_adjacency = 'data/adjacency_matrices/H_110.csv'
    run_sims(100, path_to_adjacency)
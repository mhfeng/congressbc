import numpy as np

def generate_initial_opinion_from_random_uniform(number_agents):
    return np.reshape(np.random.rand(number_agents), (number_agents, 1))

def generate_initial_opinion_from_party(number_agents, party_list):
    initial_opinions = np.zeros(number_agents)
    initial_opinions[party_list] = 1
    return initial_opinions
import csv
import numpy as np
import json

def read_adjacency_matrix(path_to_adjacency):
    with open(path_to_adjacency) as csvfile:
        reader = csv.reader(csvfile)
        next(reader)
        num_congress = sum(1 for row in reader)
    adjacency_matrix = np.zeros((num_congress, num_congress))
    i=0
    with open(path_to_adjacency) as csvfile:
        reader = csv.reader(csvfile)
        next(reader)
        for row in reader:
            adjacency_matrix[i] = row[1:]
            i += 1
    return(adjacency_matrix)

def write_model_to_json(model, model_dict, outfolder):
    filename = outfolder + str(model.id) + '.json'
    with open(filename, 'w') as outfile:
        json.dump(model_dict, outfile)